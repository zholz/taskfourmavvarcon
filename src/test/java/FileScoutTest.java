import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;


import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FileScoutTest{


    private String pathOfTestFile = "src/test/testresources/TestFile";
    private FileScout myTestFileScout =  new FileScout(pathOfTestFile, "daa", "aaaaaa");


    @Test
    public void testACounterOfMatchingAboutCorrectStartValue(){
        assertEquals(0, myTestFileScout.getCounterOfMatching());
    }
    @Test
    public void testBMatchingReplacedAboutCorrectStartValue(){
        assertEquals(false, myTestFileScout.isMatchingReplaced());
    }

    @Test
    public void testCountInScoutAndCount() {
        myTestFileScout.scoutAndCount();
        assertEquals(1, myTestFileScout.getCounterOfMatching());
    }

    @Test
    public void testDMethodScoutAndReplace() {
        myTestFileScout.scoutAndReplace();
        assertTrue(myTestFileScout.isMatchingReplaced());
    }








}