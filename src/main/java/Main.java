import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;


public class Main {
    public static void main(String[] args) throws IOException {

        OutputMessages.showUserRulesMessage();
        boolean logicRunning = true;
        while (logicRunning) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            FileScout myScout = new FileScout(reader.readLine(),reader.readLine(),reader.readLine());

            if ((!Files.isRegularFile(myScout.getPath())) || (myScout.getFindWord().isEmpty())){
                OutputMessages.showUserRulesMessage();
            }
            if ((Files.isRegularFile(myScout.getPath()))&&(myScout.getRemoveWord().isEmpty())) {
                myScout.scoutAndCount();
                logicRunning = false;
            }
            if ((Files.isRegularFile(myScout.getPath()))&&(!myScout.getRemoveWord().isEmpty())){
                myScout.scoutAndReplace();
                logicRunning = false;
            }
        }
    }
}
