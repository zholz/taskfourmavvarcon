import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.nio.file.Files.readAllLines;
import static java.nio.file.Files.write;

public class FileScout {

    private int counterOfMatching;
    private boolean isMatchingReplaced;
    private Path path;
    private String findWord;
    private String removeWord;


    public FileScout(String pathS, String findWord) {
        this.path = Paths.get(pathS);
        this.findWord = findWord;
    }

    public FileScout(String pathS, String findWord, String removeWord) {
        this.path = Paths.get(pathS);
        this.findWord = findWord;
        this.removeWord = removeWord;
    }

    public Path getPath() {
        return path;
    }

    public String getFindWord() {
        return findWord;
    }

    public String getRemoveWord() {
        return removeWord;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public void setFindWord(String findWord) {
        this.findWord = findWord;
    }

    public void setRemoveWord(String removeWord) {
        this.removeWord = removeWord;
    }

    public int getCounterOfMatching() {
        return counterOfMatching;
    }


    public boolean isMatchingReplaced() {
        return isMatchingReplaced;
    }


     private List<String> splitLinesToWords(Path filepath) {
        List<String> wordsFromSplitedLines = new ArrayList<String>();
        try {
            List <String> fileContent = new ArrayList<>(readAllLines(filepath));
            for (String s : fileContent) {
                Collections.addAll(wordsFromSplitedLines, s.split(" "));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return wordsFromSplitedLines;
    }

     void scoutAndCount() {
        int count = 0;
        List<String> resultWordsFromSplitedLines = new ArrayList<String>(splitLinesToWords(this.path));
         for (String resultWordsFromSplitedLine : resultWordsFromSplitedLines) {
             if (resultWordsFromSplitedLine.equals(this.findWord)) {
                 count++;
             }
         }
        this.counterOfMatching = count;
    }

     void scoutAndReplace() {
        List<String> resultWordsFromSplitedLines = new ArrayList<String>(splitLinesToWords(this.path));
        try {
            for (int i = 0; i < resultWordsFromSplitedLines.size(); i++) {
                if (resultWordsFromSplitedLines.get(i).equals(this.findWord)) {
                    resultWordsFromSplitedLines.set(i, this.removeWord);
                }
            }
            write(this.path, resultWordsFromSplitedLines);
            this.isMatchingReplaced = true;
        } catch (IOException e){
            System.out.println("IOExeption in scoutAndReplace() write problems");
        }
    }
}
